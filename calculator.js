/* NUR ANIS SYAMIMI BINTI ALI
   13/7/2021 */
function myFunction()
{

	var name = document.forms["form"]["name"].value;
	var length = document.forms["form"]["length"].value;
	var width = document.forms["form"]["width"].value;
	var height = document.forms["form"]["height"].value;
	var mode = document.forms["form"]["mode"].value;
	var type = document.forms["form"]["type"].value;
	var weight;
	var cost;

	weight = (length*width*height)/5000;

	alert("Parcel Volumetric and Cost Calculator"+"\nCustomer name: "+upperCase(name)+"\nLength: "
		+length+" cm"+"\nWidth: "+width+" cm"+"\nHeight: "+height+" cm"+"\nWeight: "+weight+" kg"+
		"\nMode: "+mode+"\nType: "+type+"\nDelivery cost: RM "+calculateCost(type,weight,mode));
}

function alertReset()
{
	alert("The input will be reset");
}

function upperCase(name)
{
	str = name.toUpperCase();
	return str;
}

function calculateCost(type,weight,mode)
{
	var cost;

	if (type=="Domestic")
	{
		if (weight<2.00)
		{
			if (mode=="Surface")
				cost = 7;
			else if (mode=="Air")
				cost = 10;
		}
		else
		{
			if (mode=="Surface")
				cost = 7 + ((weight-2.0)*1.5);
			else if (mode=="Air")
				cost = 10 + ((weight-2.0)*3);
		}
	}
	else if (type=="International")
	{
		if(weight<2.00)
		{
			if (mode=="Surface")
				cost = 20;
			else if (mode=="Air")
				cost = 50;
		}
		else
		{
			if(mode=="Surface")
				cost = 20 + ((weight-2.0)*3);
			else if (mode=="Air")
				cost = 50 + ((weight-2.0)*5);
		}
	}
	return cost;
}